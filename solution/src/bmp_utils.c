//#include "/home/hotach/CLionProjects/assignment-image-rotation/solution/include/bmp_utils.h"
#include "bmp_utils.h"
#include <stdbool.h>
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static uint8_t bmp_get_padding(uint64_t width) {
    if ((width % 4) == 0) return 0;
    else return 4 - (width * 3) % 4;

}

// Check readed header
static bool header_check(struct bmp_header const *bmpHeader){
    return   bmpHeader -> bfType == 0x4D42
             && bmpHeader -> bfReserved == 0
             && bmpHeader -> biSize == 40
             && bmpHeader -> bOffBits >= sizeof(struct bmp_header)
             && bmpHeader -> biPlanes == 1
             && bmpHeader -> biBitCount == 24;
}

static enum image_read_status bmp_header_read(FILE** in, struct bmp_header *bmpHeader){
    fread(bmpHeader, sizeof(struct bmp_header), 1, *in);
    if (ferror(*in)!=0) return READ_INVALID_HEADER;
    if (header_check(bmpHeader)) {
        return READ_OK;
    }
    return READ_INVALID_HEADER;
}

struct bmp_header bmp_header_create(struct image const *img) {
    struct bmp_header header;
    const uint8_t padding = bmp_get_padding(img->width);
    header = (struct bmp_header) {
            0x4D42, // bfType
            sizeof(struct bmp_header)+(img ->width* img-> height)* 3 + img ->height* padding, //bfileSize
            0, // bfReserved
            sizeof(struct bmp_header), // bOffBits
            40, // biSize
            img->width, // biWidth
            img->height, // biHeight
            1, // biPlanes
            24, // biBitCount
            0, // biCompression
            (img ->width* img-> height)* 3 + img ->height* padding, //biSizeImage
            0, // biXPelsPerMeter
            0, // biYPelsPerMeter
            3, // biClrUsed
            3}; // biClrImportant
    return header;
}
static enum image_write_status bmp_header_write(FILE** out, struct image const* image) {

    if (!*out) return OUTPUT_ERROR;

    struct bmp_header header = bmp_header_create(image);
    fwrite(&header, sizeof(struct bmp_header), 1, *out);

    if (ferror(*out)!=1) return WRITE_ERROR;
    return WRITE_OK;
}



static enum image_write_status write_bmp(FILE** out, struct image const* img ) {
    if (!*out) return OUTPUT_ERROR;

    const uint8_t padding = bmp_get_padding(img->width);

    for (uint64_t i = 0; i < img -> height; i++) {
        fwrite(img -> data + img->width*(img->height-i-1), sizeof(struct pixel), img->width, *out);// Writing img

        fseek(*out, padding, SEEK_CUR);
        if(ferror(*out) !=0){return WRITE_ERROR;}
    }
    return WRITE_OK;
}

enum image_write_status to_bmp(FILE** out, struct image* const img ) {

    if (!*out) return OUTPUT_ERROR;

    if (!bmp_header_write(out, img)) return WRITE_HEADER_ERROR;// Write headerr

    if (write_bmp(out, img)) return WRITE_ERROR; // Write bmp to file

    return WRITE_OK;
}

enum image_read_status from_bmp(FILE** in, struct image *image) {

    struct bmp_header bmpHeader;

    if (!*in){return READ_INVALID_IN;}

    enum image_read_status result = bmp_header_read(in, &bmpHeader);
    if (result != READ_OK) {
        return READ_INVALID_HEADER;
    }
//    create_image(bmpHeader.biWidth, bmpHeader.biHeight, image)// Create new image
    struct image new_image = (struct image){bmpHeader.biWidth, bmpHeader.biHeight, malloc(sizeof(struct pixel) * bmpHeader.biWidth * bmpHeader.biHeight)};

    const uint8_t padding = bmp_get_padding(bmpHeader.biWidth);

    // Reading bmp to img
    for (uint32_t i = 0; i < new_image.height; i++) {
        for (uint32_t j = 0; j < new_image.width; j++) {
            fread(new_image.data + bmpHeader.biWidth * (bmpHeader.biHeight - i - 1) + j, sizeof(struct pixel), 1, *in);
        }
        if(ferror(*in) !=0 ){
            free(new_image.data);
            return READ_INVALID_BITS;
        }
        fseek(*in,padding,SEEK_CUR);
        if(ferror(*in) !=0){
            free(new_image.data);
            return READ_INVALID_BITS;
        }
    }
    *image=new_image;

    return READ_OK;
}

