#include "error_handler.h"
//#include "/home/hotach/CLionProjects/assignment-image-rotation/solution/include/error_handler.h"
// const char* cast_enums[] ={
//         [OPEN_ERROR]="File not found\n",
//         [CLOSE_ERROR]="Cant close file\n",
//         [WRITE_ERROR]="Error while writing file\n",
//         [WRITE_HEADER_ERROR]="Error with writing header\n",
//         [OUTPUT_ERROR]="Error with output path\n",
//         [READ_INVALID_HEADER]="Error with reading header\n",
//         [READ_INVALID_IN]="Error with input path\n"


// };
enum file_open_status file_open_handler(const char* const filename, FILE** f, enum file_open_mode mode){
    enum file_open_status result = file_open(filename, f, mode);
    if(result!=OPEN_OK){
        fprintf( stderr,"%u",result);
        return result;
    }
    return OPEN_OK;
}
enum file_close_status file_close_handler(FILE** f){
    enum file_close_status result= file_close(f);
    if((result )!=CLOSE_OK){
        fprintf( stderr,"The file was not closed, error: %u\n", result );
        return result;
    }
    return CLOSE_OK;
}
enum image_read_status from_bmp_handler(FILE** in, struct image *image){
    enum image_read_status resalt =from_bmp(in, image);
    if(resalt!=READ_OK){
        fprintf( stderr,"Bmp reading error: %u\n", resalt );
        return resalt;
    }
    return READ_OK;
}
enum image_write_status to_bmp_handler(FILE** out, struct image* const img){
    enum image_write_status result = to_bmp(out, img);
    if(result!=WRITE_OK){
        fprintf( stderr,"Bmp writing error: %u" , result);
        return result;
    }
    return WRITE_OK;
}
