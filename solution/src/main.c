#include "file_operations.h"
#include "bmp_utils.h"
#include "image_utils.h"
#include "rotate.h"


#include "error_handler.h"

int main(int argc, char** argv) {
    //check input paths
    if (argc != 3) return ARGS_INVALID;

    struct image img;
    const char* const input_path = argv[1];
    char* const output_path = argv[2];

    // Open input file
    FILE* in ;
    enum file_open_status fopen_result = file_open_handler(input_path, &in, READ);
    if(fopen_result)
        return fopen_result;

    // Read image from bmp
    enum image_read_status from_bmp_result = from_bmp_handler(&in, &img);
    if(from_bmp_result)
        return from_bmp_result;

    // Close input file
    enum file_close_status file_close_result = file_close_handler(&in);
    if(file_close_result)
        return file_close_result;

//  Rotate image
    struct image img_rot = image_rotate(&img);
//  Open output file
    FILE* out;

    fopen_result = file_open_handler(output_path, &out, WRITE);
    if(fopen_result)
        return fopen_result;

//  Write image to bmp
    enum image_write_status image_write_result = to_bmp_handler(&out, &img_rot);
    if(image_write_result)
        return image_write_result;

//  Close output file

    file_close_result = file_close_handler(&out);
    if(file_close_result)
        return file_close_result;

//  Free memory
    image_free(&img);
    image_free(&img_rot);
    return 0;
}
