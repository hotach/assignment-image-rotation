#include "image_utils.h"
//#include "/home/hotach/CLionProjects/assignment-image-rotation/solution/include/image_utils.h"

struct image image_create(uint64_t width, uint64_t height) {
    struct image image = (struct image){width,height,malloc(sizeof(struct pixel) * width * height)};
    return image;
}
void image_free(struct image* image) { // Free image
    free(image -> data);
}
