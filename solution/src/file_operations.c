#include "file_operations.h"
//#include "/home/hotach/CLionProjects/assignment-image-rotation/solution/include/file_operations.h"

const char* const file_open_mode_to_char[] = {
        [READ] = "r",
        [WRITE] = "w"
};

enum file_open_status file_open(const char* const filename, FILE** f, enum file_open_mode mode ){
    if( (*f = fopen(filename, file_open_mode_to_char[mode])) == NULL ){
        return OPEN_ERROR;}
    return OPEN_OK;
}
enum file_close_status file_close(FILE** f){
    if(fclose(*f)!=0)
        return CLOSE_ERROR;
    return CLOSE_OK;
}

