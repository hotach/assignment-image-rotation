#include "rotate.h"
//#include "/home/hotach/CLionProjects/assignment-image-rotation/solution/include/rotate.h"

struct pixel get_pixel_old(const struct image *img, uint64_t i, uint64_t j) {
    return img -> data[img->width * j + i];
}
static uint32_t get_pixel_new(const struct image *img, uint64_t i, uint64_t j) {
    return img -> height * (img -> width - i- 1) + j;
}

static void set_pixel(struct image *image, uint32_t ind, struct pixel pixel) {
    image->data[ind] = pixel;
}

struct image image_rotate(struct image const* cur) {
    struct image new_image =image_create(cur -> height, cur -> width) ;// Initialising image

    for (uint32_t i = 0; i < cur -> width; i++) {
        for (uint32_t j = 0; j < cur -> height; j++) {
            set_pixel(&new_image, get_pixel_new(cur, i, j), get_pixel_old(cur, i, j));
        }
    }
    return new_image;
}
