#ifndef IMAGE_TRANSFORMER_ERROR_HANDLER_H
#define IMAGE_TRANSFORMER_ERROR_HANDLER_H
#include "bmp_utils.h"
#include "file_operations.h"
#include "image_utils.h"
#include <bits/types/FILE.h>

enum file_open_status file_open_handler(const char* const filename, FILE** f, enum file_open_mode mode);

enum file_close_status file_close_handler(FILE** f);

enum image_read_status from_bmp_handler(FILE** in, struct image *image);

enum image_write_status to_bmp_handler(FILE** out, struct image* const img);

#endif //IMAGE_TRANSFORMER_ERROR_HANDLER_H
