#ifndef IMAGE_TRANSFORMER_IMAGE_UTILS_H
#define IMAGE_TRANSFORMER_IMAGE_UTILS_H

#include "stdint.h"
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};

struct image image_create(uint64_t width, uint64_t height);

void image_free(struct image* image);

#endif //IMAGE_TRANSFORMER_IMAGE_UTILS_H
