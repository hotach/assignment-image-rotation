#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image_utils.h"
#include <stdbool.h>

struct image image_rotate(struct image const* cur);

#endif //IMAGE_TRANSFORMER_ROTATE_H
