#ifndef IMAGE_TRANSFORMER_FILE_OPERATIONS_H
#define IMAGE_TRANSFORMER_FILE_OPERATIONS_H
#include "open_close_enums.h"
#include <bits/types/FILE.h>
#include <stdio.h>
#include <string.h>
enum file_open_mode{
    READ,
    WRITE
};
enum file_open_status file_open(const char* const filename, FILE** f, enum file_open_mode mode );

enum file_close_status file_close(FILE** f);

#endif //IMAGE_TRANSFORMER_FILE_OPERATIONS_H
