#ifndef IMAGE_TRANSFORMER_OPEN_CLOSE_ENUMS_H
#define IMAGE_TRANSFORMER_OPEN_CLOSE_ENUMS_H

enum file_open_status{
    OPEN_OK=0,
    OPEN_ERROR,
};
enum file_close_status{
    CLOSE_OK = 0,
    CLOSE_ERROR,
};
enum image_read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_IN,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};
enum  image_write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    OUTPUT_ERROR
    /* коды других ошибок  */
};
enum read_args_status{
    ARGS_OK=0,
    ARGS_INVALID
};

#endif
