#ifndef IMAGE_TRANSFORMER_BMP_UTILS_H
#define IMAGE_TRANSFORMER_BMP_UTILS_H

#include "image_utils.h"
#include "open_close_enums.h"

#include "malloc.h"
#include "stdio.h"

enum image_write_status to_bmp(FILE** out, struct image* img ) ;

enum image_read_status from_bmp(FILE** in, struct image *image) ;

#endif //IMAGE_TRANSFORMER_BMP_UTILS_H
